import { put, takeLatest } from "redux-saga/effects";
import actionTypes from "../action/ActionTypes";
import * as Jobs from "../action/jobs";

function* getAllJob(data) {
  const { offset } = data;
  const response = yield Jobs.getAllJobs(offset);
  if (response) {
    var params = new URLSearchParams(
      response
        .pop()
        .links.pop()
        .href.split("?")[1]
    );
    let res = [];
    if (response.length !== 0)
      yield Promise.all(
        response.map(async (item) => {
          const res = await Jobs.getJobSkillId(item.uuid);
          return res;
        })
      ).then((result) => (res = result));
    yield put({
      type: actionTypes.GET_JOBS_RECEIVED,
      payload: {
        response: res[0] ? res : response,
        offset: params.get("offset"),
      },
    });
  }
}

function* getDetailsJob(data) {
  const { id } = data;
  const response = yield Jobs.getJobSkillId(id);

  yield put({
    type: actionTypes.GET_DETAILS_JOB_RECEIVED,
    payload: response,
  });
}

function* getRelatedJobs(data) {
  const { id } = data;
  const response = yield Jobs.getRelatedJobs(id);

  yield put({
    type: actionTypes.GET_RELATED_JOB_RECEIVED,
    payload: response.related_job_titles || [],
  });
}

function* getAutoComplete(data) {
  try {
    const { word } = data;
    const response = yield Jobs.getAutoComplete(word);

    yield put({
      type: actionTypes.GET_AUTOCOMPLETE_JOB_RECEIVED,
      payload: response.map((item) => item.suggestion) || [],
    });
  } catch (err) {
    yield put({
      type: actionTypes.GET_AUTOCOMPLETE_JOB_RECEIVED,
      payload: [],
    });
  }
}

function* getSearchJob(data) {
  const { search } = data;
  const response = yield Jobs.getAutoComplete(search);
  if (response) {
    let res;
    if (response.length !== 0)
      yield Promise.all(
        response.map(async (item) => {
          const res = await Jobs.getJobSkillId(item.uuid);
          return res;
        })
      ).then((result) => (res = result));
    yield put({
      type: actionTypes.GET_SEARCH_JOBS_RECEIVED,
      payload: { response: res[0] ? res : response },
    });
  }
}

export default function* jobsWatcher() {
  yield takeLatest(actionTypes.GET_JOBS_REQUEST, getAllJob);
  yield takeLatest(actionTypes.GET_DETAILS_JOB_REQUEST, getDetailsJob);
  yield takeLatest(actionTypes.GET_RELATED_JOB_REQUEST, getRelatedJobs);
  yield takeLatest(actionTypes.GET_AUTOCOMPLETE_JOB_REQUEST, getAutoComplete);
  yield takeLatest(actionTypes.GET_SEARCH_JOBS_REQUEST, getSearchJob);
}
