import { all, fork } from "redux-saga/effects";
import jobsWatcher from "./jobs";
import skillsWatcher from "./skill";

export default function* rootSaga() {
  yield all([fork(jobsWatcher), fork(skillsWatcher)]);
}
