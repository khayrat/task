import { put, takeLatest } from "redux-saga/effects";
import actionTypes from "../action/ActionTypes";
import * as Skill from "../action/Skill";

function* getDetailsSkill(data) {
  const { id } = data;
  const response = yield Skill.getSkillDetails(id);
  const resp = yield Skill.getSkillJobRelated(id);
  yield put({
    type: actionTypes.GET_DETAILS_SKILL_RECEIVED,
    payload: { ...response, jobs: resp.jobs },
  });
}

function* getRelatedSkills(data) {
  const { id } = data;
  const response = yield Skill.getRelatedSkills(id);
  yield put({
    type: actionTypes.GET_RELATED_SKILL_RECEIVED,
    payload: response.skills || [],
  });
}

export default function* skillsWatcher() {
  yield takeLatest(actionTypes.GET_DETAILS_SKILL_REQUEST, getDetailsSkill);
  yield takeLatest(actionTypes.GET_RELATED_SKILL_REQUEST, getRelatedSkills);
}
