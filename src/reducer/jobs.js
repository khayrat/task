import actionTypes from "../action/ActionTypes";

const initialState = {
  allJobs: [],
  currentOffset: 0,
  totalJobs: 0,
  nextOffset: 12,
  details: null,
  relatedJobs: [],
  suggestions: [],
  loading: true,
  historySearch: [],
};

export default function(state = initialState, action) {
  switch (action.type) {
    case actionTypes.GET_JOBS_REQUEST:
      return {
        ...state,
        allJobs: [...state.allJobs],
        currentOffset: action.offset,
      };
    case actionTypes.GET_JOBS_RECEIVED:
      return {
        ...state,
        allJobs: [...state.allJobs, ...action.payload.response],
        nextOffset: state.currentOffset + 12,
        totalJobs: parseInt(action.payload.offset) + 12,
      };
    case actionTypes.GET_DETAILS_JOB_REQUEST:
      return {
        ...state,
        details: null,
      };
    case actionTypes.GET_DETAILS_JOB_RECEIVED:
      return {
        ...state,
        details: { ...action.payload },
      };
    case actionTypes.GET_RELATED_JOB_REQUEST:
      return {
        ...state,
        relatedJobs: [],
      };
    case actionTypes.GET_RELATED_JOB_RECEIVED:
      return {
        ...state,
        relatedJobs: [...action.payload],
      };
    case actionTypes.GET_AUTOCOMPLETE_JOB_REQUEST:
      return {
        ...state,
        loading: true,
        suggestions: [],
      };
    case actionTypes.GET_AUTOCOMPLETE_JOB_RECEIVED:
      return {
        ...state,
        loading: false,
        suggestions: [...action.payload],
      };
    case actionTypes.GET_SEARCH_JOBS_REQUEST:
      let newHistorySearch;
      if (
        state.historySearch.findIndex((item) => item === action.search) === -1
      ) {
        newHistorySearch = [...state.historySearch, action.search];
      } else {
        newHistorySearch = state.historySearch;
      }
      return {
        ...state,
        allJobs: [],
        currentOffset: -1,
        historySearch: newHistorySearch,
      };
    case actionTypes.GET_SEARCH_JOBS_RECEIVED:
      return {
        ...state,
        allJobs: [...state.allJobs, ...action.payload.response],
        nextOffset: -1,
        totalJobs: action.payload.response.length,
      };
    default:
      return state;
  }
}
