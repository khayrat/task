import { combineReducers } from "redux";
import jobs from "./jobs";
import skill from "./skill";

export default combineReducers({
  jobs,
  skill,
});
