import actionTypes from "../action/ActionTypes";

const initialState = {
  details: null,
  relatedSkill: [],
};

export default function (state = initialState, action) {
  switch (action.type) {
    case actionTypes.GET_DETAILS_SKILL_REQUEST:
      return {
        ...state,
        details: null,
      };
    case actionTypes.GET_DETAILS_SKILL_RECEIVED:
      return {
        ...state,
        details: { ...action.payload },
      };
    case actionTypes.GET_RELATED_SKILL_REQUEST:
      return {
        ...state,
        relatedSkill: [],
      };
    case actionTypes.GET_RELATED_SKILL_RECEIVED:
      return {
        ...state,
        relatedSkill: [...action.payload],
      };
    default:
      return state;
  }
}
