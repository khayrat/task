import http from "../http.js";
import notify from "../component/toaster/index";

export const getAllJobs = async (offset) => {
  try {
    const response = await http.get(`/jobs?offset=${offset}&limit=12`);
    return response;
  } catch (e) {
    notify(e.message);
    return null;
  }
};

export const getJobSkillId = async (id) => {
  try {
    const response = await http.get(`/jobs/${id}/related_skills`);
    return response;
  } catch (e) {
    notify(e.message);
  }
};

export const getRelatedJobs = async (id) => {
  try {
    const response = await http.get(`/jobs/${id}/related_jobs`);
    return response;
  } catch (e) {
    notify(e.message);
    return null;
  }
};

export const getAutoComplete = async (word) => {
  try {
    const response = await http.get(`/jobs/autocomplete?contains=${word}`);
    return response;
  } catch (e) {
    return null;
  }
};
