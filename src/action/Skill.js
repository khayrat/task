import http from "../http.js";
import notify from "../component/toaster/index";

export const getSkillDetails = async (id) => {
  try {
    const response = await http.get(`/skills/${id}`);
    return response;
  } catch (e) {
    notify(e.message);
    return null;
  }
};

export const getSkillJobRelated = async (id) => {
  try {
    const response = await http.get(`/skills/${id}/related_jobs`);
    return response;
  } catch (e) {
    notify(e.message);
    return null;
  }
};

export const getRelatedSkills = async (id) => {
  try {
    const response = await http.get(`/skills/${id}/related_skills`);
    return response;
  } catch (e) {
    notify(e.message);
    return null;
  }
};
