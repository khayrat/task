import actionTypes from "./ActionTypes";

export const getAllJobs = (offset) => ({
  type: actionTypes.GET_JOBS_REQUEST,
  offset,
});

export const getJobDetails = (id) => ({
  type: actionTypes.GET_DETAILS_JOB_REQUEST,
  id,
});

export const setJobDetails = (payload) => ({
  type: actionTypes.GET_DETAILS_JOB_RECEIVED,
  payload,
});

export const relatedJobs = (id) => ({
  type: actionTypes.GET_RELATED_JOB_REQUEST,
  id,
});

export const getSkillDetails = (id) => ({
  type: actionTypes.GET_DETAILS_SKILL_REQUEST,
  id,
});

export const relatedSkill = (id) => ({
  type: actionTypes.GET_RELATED_SKILL_REQUEST,
  id,
});

export const getAutoComplete = (word) => ({
  type: actionTypes.GET_AUTOCOMPLETE_JOB_REQUEST,
  word,
});

export const getSearchJobs = (search) => ({
  type: actionTypes.GET_SEARCH_JOBS_REQUEST,
  search,
});
