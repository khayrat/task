import React from "react";
import { Route, Switch, Router } from "react-router-dom";
import Home from "./component/Home/Home";
import JobDetails from "./component/JobDetails/JobDetails";
import SkillDetails from "./component/SkillDetails/SkillDetails";

import "./App.scss";
import "react-toastify/dist/ReactToastify.css";
import { createBrowserHistory } from "history";
const history = createBrowserHistory();

export default function App() {
  const loading = () => <div>loading</div>;

  return (
    <Router history={history}>
      <React.Suspense fallback={loading()}>
        <Switch>
          <Route exact path="/job-details/:id" render={() => <JobDetails />} />
          <Route
            exact
            path="/skill-details/:skillId"
            render={() => <SkillDetails />}
          />
          <Route exact path="/" render={() => <Home />} />
          <Route exact path="/search" render={() => <Home />} />
        </Switch>
      </React.Suspense>
    </Router>
  );
}
