import React, { useEffect } from "react";
import { withRouter } from "react-router";
import { connect } from "react-redux";
import { relatedSkill, getSkillDetails } from "../../action/index";
import { CARD_TYPE_JOB } from "../../Constant/job";

import Nav from "../Nav/Nav";
import Card from "../Card/Card";

import "./SkillDetails.scss";

const SkillDetails = (props) => {
  const {
    match: {
      params: { skillId },
    },
    fetchRelatedSkill,
    relatedSkill,
    fetchSkillDetails,
    details,
  } = props;

  useEffect(() => {
    fetchSkillDetails(skillId);
    fetchRelatedSkill(skillId);
  }, [skillId, fetchRelatedSkill, fetchSkillDetails]);

  const handleClickDetails = (itemId) => {
    const { history } = props;
    history.push(`/skill-details/${itemId}`);
  };

  return (
    <>
      <Nav />
      <div className="br-skill-details-container">
        <p className="br-skill-details-container__title">
          {details?.skill_name}
        </p>
        <div className="br-skill-details-container__row">
          <div className="br-skill-details-container__row__related-job">
            <p>Description:</p>
            <div className="br-skill-details-container__row__related-job__description">
              {details?.description}
            </div>
            <p>Related Jobs:</p>
            {details?.jobs?.map((item) => (
              <Card item={item} type={CARD_TYPE_JOB} key={item.job_uuid} />
            ))}
          </div>
          <div className="br-skill-details-csontainer">
            <p>Related Skills:</p>
            <ul>
              {relatedSkill?.map((item) => (
                <li
                  onClick={() => handleClickDetails(item.uuid)}
                  key={item.uuid}
                >
                  {item.skill_name}
                </li>
              ))}
            </ul>
          </div>
        </div>
      </div>
    </>
  );
};

const mapDispatchToProps = (dispatch) => ({
  fetchSkillDetails: (skillId) => dispatch(getSkillDetails(skillId)),
  fetchRelatedSkill: (skillId) => dispatch(relatedSkill(skillId)),
});

const mapStateToProps = ({ skill }) => {
  return skill;
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(SkillDetails));
