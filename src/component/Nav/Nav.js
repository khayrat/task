import React, { useState } from "react";
import { withRouter } from "react-router";

import { ReactComponent as CloseMenu } from "../../assets/x.svg";
import { ReactComponent as MenuIcon } from "../../assets/menu.svg";

import "./Nav.scss";

const Nav = (props) => {
  const [click, setClick] = useState(false);

  const handleClick = () => setClick(!click);

  const handleClickHome = () => {
    const { history } = props;
    history.push(`/`);
  };

  const handleClickSearch = () => {
    const { history } = props;
    history.push(`/search`);
  };

  const renderMenu = () => (
    <ul className={click ? "br-nav__menu-items  active" : "br-nav__menu-items"}>
      <li>
        <span onClick={() => handleClickHome()}>Home</span>
      </li>
      <li>
        <span onClick={() => handleClickSearch()}>Search</span>
      </li>
      <li>
        <span>History</span>
      </li>
    </ul>
  );

  return (
    <div className="br-nav">
      <div className="br-nav__logo-nav">
        <div className="br-nav__logo-nav__logo-container">JobNow</div>
        {renderMenu()}
      </div>
      {renderMenu()}
      <div className="br-nav__mobile-menu" onClick={handleClick}>
        {click ? (
          <CloseMenu className="br-nav__menu-icon" />
        ) : (
          <MenuIcon className="br-nav__menu-icon" />
        )}
      </div>
    </div>
  );
};

export default withRouter(Nav);
