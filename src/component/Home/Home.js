import React, { useEffect } from "react";
import { withRouter } from "react-router";
import { connect } from "react-redux";
import { getAllJobs, getSearchJobs } from "../../action/index";

import Nav from "../Nav/Nav";
import Header from "../Header/Header";
import InfiniteScroll from "react-infinite-scroll-component";
import Card from "../Card/Card";

import "./Home.scss";

const Home = (props) => {
  const {
    fetchJobs,
    allJobs,
    nextOffset,
    totalJobs,
    historySearch,
    history,
    fetchSearch,
  } = props;

  useEffect(() => {
    fetchJobs(0);
  }, [fetchJobs]);

  return (
    <>
      <Nav />
      <Header />
      <div className="br-display-container">
        <div className="br-home-container">
          <p className="br-home-container__title">All Jobs ({totalJobs})</p>
          <InfiniteScroll
            dataLength={allJobs?.length}
            next={() => {
              if (nextOffset !== -1) fetchJobs(nextOffset);
            }}
            hasMore={true}
            loader={<h4>Loading...</h4>}
            endMessage={
              <p style={{ textAlign: "center" }}>
                <b>Yay! You have seen it all</b>
              </p>
            }
          >
            {allJobs?.map(
              (item) => item && <Card item={item} key={item?.job_uuid} />
            )}
          </InfiniteScroll>
        </div>
        {historySearch?.length > 0 && (
          <div className="br-job-details-csontainer">
            <p>history search:</p>

            <ul>
              {historySearch.map((item, index) => (
                <li
                  key={index}
                  onClick={() => {
                    fetchSearch(item);

                    history.replace({
                      search: `/search?query=${item}`,
                    });
                  }}
                >
                  {item}
                </li>
              ))}
            </ul>
          </div>
        )}
      </div>
    </>
  );
};

const mapDispatchToProps = (dispatch) => ({
  fetchJobs: (offset) => dispatch(getAllJobs(offset)),
  fetchSearch: (word) => dispatch(getSearchJobs(word)),
});

const mapStateToProps = ({ jobs }) => {
  return jobs;
};

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Home));
