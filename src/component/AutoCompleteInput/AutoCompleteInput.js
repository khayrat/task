import React, { Fragment, useEffect, useState } from "react";
import { connect } from "react-redux";
import { getAutoComplete, getSearchJobs } from "../../action/index";
import { withRouter } from "react-router";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSearch } from "@fortawesome/free-solid-svg-icons";

const AutoCompleteInput = (props) => {
  const [activeSuggestion, setActiveSuggestion] = useState(0);
  const [filteredSuggestions, setFilteredSuggestions] = useState([]);
  const [showSuggestions, setShowSuggestions] = useState(false);
  const [userInput, setUserInput] = useState("");
  const { suggestions, fetchSearch, history } = props;

  useEffect(() => {
    setFilteredSuggestions(suggestions);
  }, [suggestions]);

  useEffect(() => {
    if (userInput?.length < 3) {
      setFilteredSuggestions([]);
    }
    if (userInput.length === 0)
      history.replace({
        search: null,
      });
  }, [userInput, history]);

  const onChange = (e) => {
    const { fetchAutoComplete } = props;
    const userInput = e.currentTarget.value;
    if (userInput.length >= 3) {
      setActiveSuggestion(0);
      setShowSuggestions(true);
      fetchAutoComplete(userInput);
    }
    setUserInput(e.currentTarget.value);
  };

  const onClick = (e) => {
    setActiveSuggestion(0);
    setFilteredSuggestions([]);
    setShowSuggestions(false);
    setUserInput(e.currentTarget.innerText);
  };

  const onKeyDown = (e) => {
    if (e.keyCode === 13) {
      setActiveSuggestion(0);
      setShowSuggestions(false);
      setUserInput(
        filteredSuggestions[activeSuggestion] || e.currentTarget.value
      );
    } else if (e.keyCode === 38) {
      if (activeSuggestion === 0) {
        return;
      }
      setActiveSuggestion(activeSuggestion - 1);
    } else if (e.keyCode === 40) {
      if (activeSuggestion - 1 === filteredSuggestions.length) {
        return;
      }
      setActiveSuggestion(activeSuggestion + 1);
    }
  };

  const showSuggestionsListComponent = () => {
    const { loading } = props;
    if (showSuggestions && userInput) {
      if (filteredSuggestions.length) {
        return (
          <ul className="br-input-suggest">
            {filteredSuggestions.map((suggestion, index) => {
              let className;
              if (index === activeSuggestion) {
                className = "br-input-suggest-active";
              }
              return (
                <li className={className} key={suggestion} onClick={onClick}>
                  {suggestion}
                </li>
              );
            })}
          </ul>
        );
      } else {
        return (
          !loading && (
            <div className="no-br-input-suggest ">No suggestions Found</div>
          )
        );
      }
    }
  };

  return (
    <Fragment>
      <div className="br-header-container__input-with-icon">
        <FontAwesomeIcon
          className="icon"
          icon={faSearch}
          onClick={() => {
            if (userInput.length >= 3) {
              fetchSearch(userInput);

              history.replace({
                search: `?query=${userInput}`,
              });
            }
          }}
        />
        <input
          type="text"
          onChange={onChange}
          onKeyDown={onKeyDown}
          value={userInput}
          className="br-header-container__search__input"
          placeholder="Search Keyword"
        />
        {showSuggestionsListComponent()}
      </div>
    </Fragment>
  );
};

const mapDispatchToProps = (dispatch) => ({
  fetchAutoComplete: (word) => dispatch(getAutoComplete(word)),
  fetchSearch: (word) => dispatch(getSearchJobs(word)),
});

const mapStateToProps = ({ jobs }) => {
  return jobs;
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(AutoCompleteInput));
