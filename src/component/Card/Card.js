import React from "react";
import { withRouter } from "react-router";

import { CARD_TYPE_JOB } from "../../Constant/job";

import Tags from "../Tags/Tags";

import "./Card.scss";

const card = (props) => {
  const { item, type } = props;

  const handleClickDetails = (itemId) => {
    const { history } = props;
    history.push(`/job-details/${itemId}`);
  };

  const handleClickDetailsSkill = (itemId) => {
    const { history } = props;
    history.push(`/skill-details/${itemId}`);
  };

  return (
    <>
      {item && (
        <div className="br-card-container">
          <div className="br-card-container__content">
            <div className="br-card-container__title">
              <p
                onClick={() =>
                  type
                    ? type !== CARD_TYPE_JOB
                      ? handleClickDetailsSkill(item.skill_uuid)
                      : handleClickDetails(item.job_uuid)
                    : ""
                }
              >
                {item.job_title ||
                  item.title ||
                  item.skill_name ||
                  item.suggestion}
              </p>
            </div>

            {type && (
              <>
                <div className="br-card-container__description">
                  <p>{item.description}</p>
                </div>

                <div className="br-card-container__tags-number">
                  {item.skill_type && (
                    <p>
                      <span>Type: </span>
                      {item.skill_type}
                    </p>
                  )}
                  {item.importance && (
                    <p>
                      <span>Importance: </span>
                      {item.importance}
                    </p>
                  )}
                  {item.level && (
                    <p>
                      <span>Level: </span>
                      {item.level}
                    </p>
                  )}
                </div>
              </>
            )}
            {!type && (
              <>
                <div>
                  <p>Related Skills:</p>
                </div>
                <div className="br-card-container__tags">
                  {item?.skills?.slice(0, 6).map((skill) => (
                    <Tags skill={skill} key={skill.skill_uuid} />
                  ))}
                </div>

                <div className="br-card-container__link">
                  <div
                    onClick={() =>
                      handleClickDetails(item.job_uuid || item.uuid)
                    }
                  >
                    View Job Details
                  </div>
                </div>
              </>
            )}
          </div>
        </div>
      )}
    </>
  );
};

export default withRouter(card);
