import React, { useEffect } from "react";
import { withRouter } from "react-router";
import { connect } from "react-redux";
import { getJobDetails, setJobDetails, relatedJobs } from "../../action/index";

import Nav from "../Nav/Nav";
import Card from "../Card/Card";

import "./JobDetails.scss";

const JobDetails = (props) => {
  const {
    details,
    relatedJobs,
    match: {
      params: { id },
    },
    allJobs,
    fetchDetailsJobs,
    setDetails,
    fetchRelatedJobs,
  } = props;

  useEffect(() => {
    if (id) {
      const job = allJobs.find((item) => item.job_uuid === id);
      if (job) {
        setDetails(job);
      } else {
        fetchDetailsJobs(id);
      }
      fetchRelatedJobs(id);
    }
  }, [id, allJobs, fetchDetailsJobs, fetchRelatedJobs, setDetails]);

  const handleClickDetails = (itemId) => {
    const { history } = props;
    history.push(`/job-details/${itemId}`);
  };

  return (
    <>
      <Nav />
      <div className="br-job-details-container">
        <p className="br-job-details-container__title">{details?.job_title}</p>
        <div className="br-job-details-container__row">
          <div className="br-job-details-container__row__related-job">
            <p>Related Skills:</p>
            {details?.skills?.map((item) => (
              <Card item={item} type="details" key={item.skill_uuid} />
            ))}
          </div>
          <div className="br-job-details-csontainer">
            <p>Related Jobs:</p>

            <ul>
              {relatedJobs.map((item) => (
                <li
                  onClick={() => handleClickDetails(item.uuid)}
                  key={item.uuid}
                >
                  {item.title}
                </li>
              ))}
            </ul>
          </div>
        </div>
      </div>
    </>
  );
};

const mapDispatchToProps = (dispatch) => ({
  fetchDetailsJobs: (id) => dispatch(getJobDetails(id)),
  setDetails: (data) => dispatch(setJobDetails(data)),
  fetchRelatedJobs: (id) => dispatch(relatedJobs(id)),
});

const mapStateToProps = ({ jobs }) => {
  return jobs;
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(JobDetails));
