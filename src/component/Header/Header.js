import React from "react";

import AutoCompleteInput from "../AutoCompleteInput/AutoCompleteInput";

import "./Header.scss";

const Header = () => {
  return (
    <div className="br-header-container__search">
      <AutoCompleteInput />
    </div>
  );
};

export default Header;
