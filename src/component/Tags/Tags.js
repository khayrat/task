import React from "react";

import "./Tags.scss";

const Tags = (props) => {
  const { skill } = props;

  return (
    <>
      {skill && (
        <div className="br-tag-container">
          <div className="br-tag-container__content">
            <p>{skill.skill_name}</p>
          </div>
        </div>
      )}
    </>
  );
};

export default Tags;
